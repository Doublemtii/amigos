package com.amigoscode.clients.notification;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "notification-service",
        url = "${clients.notification.url}",
        path = "api/v1/notification"
)
public interface NotificationClient {

    @PostMapping
    void sendNotification(@RequestBody String email);
}
