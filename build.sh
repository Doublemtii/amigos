#!/bin/bash

curDir="$(pwd)"


build() {
  echo "Building ${1} image ..."
  cd "${curDir}"/"${1}"
  mvn package spring-boot:repackage && cd build && docker build -t mansourthior/${1}:latest -f Dockerfile .
  cd ..
  cd ..
}

build "customer"
build "fraud"
build "notification"

docker-compose down
docker-compose up -d