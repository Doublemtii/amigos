package com.amigos.customer;

public record CustomerRequest(String firstName,
                              String lastName,
                              String email) {
}
