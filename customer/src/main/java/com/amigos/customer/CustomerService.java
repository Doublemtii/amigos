package com.amigos.customer;

import com.amigos.amqp.RabbitMQMessageProducer;
import com.amigoscode.clients.fraud.FraudCheckResponse;
import com.amigoscode.clients.fraud.FraudClient;
import com.amigoscode.clients.notification.NotificationClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
//    private final RestTemplate restTemplate;

    private final RabbitMQMessageProducer producer;

    private final NotificationConfig config;
    private final FraudClient fraudClient;
    private final NotificationClient notificationClient;

    public void register(CustomerRequest customerRequest) {
        var customer = Customer.builder()
                .email(customerRequest.email())
                .firstName(customerRequest.firstName())
                .lastName(customerRequest.lastName())
                .build();
        customerRepository.saveAndFlush(customer);

    /*
        FraudCheckResponse fraudCheckResponse = restTemplate.getForObject(
                "http://FRAUD-SERVICE/api/v1/fraud-check/{customerId}",
                    FraudCheckResponse.class,
                    customer.getId()
         );
    */
        FraudCheckResponse fraudCheckResponse = fraudClient.isFraudster(customer.getId());


        assert fraudCheckResponse != null;
        if(fraudCheckResponse.isFraudster()) {
            throw new IllegalStateException("Fraudster");
        }

        // Publish the object
        producer.publish(
                customerRequest.email(),
                config.getInternalExchange(),
                config.getInternalNotificationRoutingKeys()
        );
        //var result = notificationClient.sendNotification(customerRequest.email());
        //log.info("RESULT ==> {}", result);
    }
}
