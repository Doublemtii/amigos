package com.amigos.notification.rabbitmq;

import com.amigos.notification.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class NotificationConsumer {

    private final NotificationService notificationService;

    // Consume the object in the queue
    @RabbitListener(queues = "${rabbitmq.queues.notification}")
    public void consume(String email) {
        log.info("Consume {} from queue", email);
        notificationService.saveNotification(email);
    }
}
