package com.amigos.notification;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationService {

    private final NotificationRepository notificationRepository;

    public void saveNotification(String email) {
        log.info("Creating notification for {}", email);
        var notification =  Notification.builder()
                .date(LocalDateTime.now())
                .email(email)
                .message("The owner of this account is a fraudster")
                .build();
        notificationRepository.save(notification);
    }
}
