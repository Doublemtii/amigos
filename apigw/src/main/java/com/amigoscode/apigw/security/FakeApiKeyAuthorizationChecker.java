package com.amigoscode.apigw.security;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("fake")
public class FakeApiKeyAuthorizationChecker implements ApiKeyAuthorizationChecker {

    // supersecure is the key given in the header
    private final static Map<String, List<String>> keys = Map.of(
      "supersecure", List.of("customer")
    );

    @Override
    public boolean isAuthenticated(String key, String application) {
        return keys.getOrDefault(key, List.of())
                .stream()
                .anyMatch(k -> k.contains(application));
    }
}
