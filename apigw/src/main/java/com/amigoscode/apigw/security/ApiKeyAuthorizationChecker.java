package com.amigoscode.apigw.security;

public interface ApiKeyAuthorizationChecker {

    boolean isAuthenticated(String key, String application);

}
