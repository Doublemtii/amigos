package com.amigoscode.apigw.security;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class ApiKeyAuthorization implements GlobalFilter, Ordered {

    private final FakeApiKeyAuthorizationChecker fakeApiKeyAuthorizationChecker;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        Route route = exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
        String application = route != null ? route.getId() : null;


        // ApiKey is added to the postman request in the headers
        List<String> apiKey = exchange.getRequest().getHeaders().get("ApiKey");
        boolean isAuthorized = apiKey != null && fakeApiKeyAuthorizationChecker.isAuthenticated(apiKey.get(0), application);

        if (application == null || Objects.requireNonNull(apiKey).isEmpty() || !isAuthorized)
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You're not authorized");

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
