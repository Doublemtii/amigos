package com.amigos.fraud;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableEurekaClient
@PropertySources(
        @PropertySource("classpath:clients-${spring.profiles.active}.properties")
)
public class FraudApplication {

    public static void main(String[] args) {
        SpringApplication.run(FraudApplication.class, args);
    }

//    // Test Function
//    @Bean
//    CommandLineRunner commandLineRunner() {
//        return args -> {
//            Function<Integer, Integer> s = x -> x * x;
//            var list = new ArrayList<>(Arrays.asList(1, 2, 3));
//            System.out.println(list.stream().map(s).collect(Collectors.toList()));
//        };
//    }
}
