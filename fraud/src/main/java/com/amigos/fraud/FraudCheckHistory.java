package com.amigos.fraud;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "fraud_check_history")
public class FraudCheckHistory {


    @Id
    @SequenceGenerator(name = "fraud_id_sequence", sequenceName = "fraud_id_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fraud_id_sequence")
    @Column(name = "id")
    private Long id;

    private Long customerId;

    private Boolean isFraudster;

    private LocalDateTime createdAt;

}
