package com.amigos.fraud;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class FraudService {

    private final FraudRepository fraudRepository;

    public boolean isFraudulentCustomer(Long customerId) {
        fraudRepository.save(
              FraudCheckHistory.builder()
                      .customerId(customerId)
                      .isFraudster(false)
                      .createdAt(LocalDateTime.now())
                      .build()
        );
        return false;
    }
}
